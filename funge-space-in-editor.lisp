(in-package :toadstool)

(defparameter *untitled-counter* 0)

(defclass funge-instance-in-editor (funge:funge-instance
                                    funge:funge-space-in-multiverse)
  ((funge-space-offset
    :accessor funge-space-offset
    :documentation "Offset of the viewed area in the
    editor. FUNGE-SPACE-OFFSET represents the most-left/upper
    corner.")
   (cursor
    :accessor cursor
    :documentation "Position of the cursor.")
   ;; (orientation-cursor
   ;;  :accessor orientation-cursor)
   (orientation-delta
    :accessor orientation-delta
    :documentation "A quasi-delta that indicates how the cursor moves,
    if typing and other operations as search, etc. move.")
   (name
    :initarg :name
    :accessor name
    :initform (format nil "untitled-~A" (incf *untitled-counter*))
    :documentation "Name of the funge-space instance.")
   (full-file-name
    :initarg :full-file-name
    :accessor full-file-name
    :initform nil
    :documentation "Full pathname of the funge-space instance.")
   (has-changed-p
    :accessor has-changed-p
    :initform nil
    :documentation "Whether the funge-space needs save.")
   (insert-mode
    :accessor insert-mode
    :initform :overwrite
    :documentation "When true, typing text inserts, otherwise overwrites.")
   (undo-list
    :accessor undo-list
    :initform nil
    :documentation "The undo-list (list of 'reverse' commands of
    ongoing editing.")
   (search-result
    :accessor search-result
    :initform nil
    :documentation "Contains the results (location as list) of the
    last search.")
   (search-result-position
    :accessor search-result-position
    :initform 0
    :documentation "Which search result (out ouf search-result) the
    cursor will jump to.")
   (mark-aktive-p
    :accessor mark-aktive-p
    :initform nil
    :documentation "If the the mark is active the region will be
    highlighted.")
   (mark
    :accessor mark
    :initform nil
    :documentation "Position of the mark.")
   (breakpoints
    :accessor breakpoints
    :initform nil
    :documentation "List of vectors representing breakpoints. When the
    IP hits them, the execution of the funge-instance stops.")))

(defmethod initialize-instance :after ((fi funge-instance-in-editor) &key)
  (setf (funge-space-offset fi) (make-n-list (dimensions fi)))
  (setf (cursor fi) (make-n-list (dimensions fi) 2))
  (setf (orientation-delta fi)
        (let ((l (make-n-list (dimensions fi))))
          (setf (x l) 1)
          l))
  (setf (what-todo-when-finished fi)
        (lambda (fi)
          (declare (ignore fi))
          (format t "Last thread finished.")
          (signal 'exit-running-funge-instance))))

(defmethod orientation-cardinal-p ((fs funge-instance-in-editor))
  "Is the orientation of the cursor cardinal (T) or flying (NIL)?"
  (= (abs (loop for i in (orientation-delta fs) sum i)) 1))

(defmethod actual-cell-coords-beyond-cursor ((fi
                                              funge-instance-in-editor))
  (let ((return-vector (copy-seq (funge-space-offset fi))))
    (incf (x return-vector) (x (cursor fi)))
    (incf (y return-vector) (y (cursor fi)))
    return-vector))

(defmethod (setf actual-cell-coords-beyond-cursor)
    (coords
     (fi
      funge-instance-in-editor))
  (setf (cursor fi)
        (subseq
         (+-vector
          (cursor fi)
          (--vector
           coords
           (actual-cell-coords-beyond-cursor fi)))
         0 (if (> (dimensions fi) 1)
               2 1)))
  (when (> (dimensions fi) 2)
    (loop for i from 2 to (1- (dimensions fi))
       do
         (setf (nth i (funge-space-offset fi))
               (nth i coords)))))

(defmethod actual-cell-coords-beyond-xy ((fi funge-instance-in-editor)
                                         x y)
  (let ((return-vector (copy-seq (funge-space-offset fi))))
    (incf (x return-vector) x)
    (when (> (dimensions fi) 1)
      (incf (y return-vector) y))
    return-vector))

(defmethod actual-cell-coords-beyond-cursor ((fi
                                              funge-instance-in-editor))
  (if (> (dimensions fi) 1)
      (actual-cell-coords-beyond-xy fi (x (cursor fi)) (y (cursor fi)))
      (actual-cell-coords-beyond-xy fi (x (cursor fi)) nil)))

(defmethod actual-cell-beyond-cursor-xy ((fi funge-instance-in-editor)
                                         xcursor ycursor)
  (let ((return-vector (copy-seq (funge-space-offset fi))))
    (incf (x return-vector) xcursor)
    (when (> (dimensions fi) 1)
      (incf (y return-vector) ycursor))
    (cell* fi return-vector)))

(defmethod actual-cell-beyond-cursor ((fi funge-instance-in-editor))
  (actual-cell-beyond-cursor-xy fi (x (cursor fi)) (y (cursor fi))))

(defun handle-cursor-if-out-of-view (funge-instance )
  (when (> (x (actual-cell-coords-beyond-cursor funge-instance))
           (+ (x (funge-space-offset funge-instance))
              (- (tui:width (editor-window *app*)) 3)))
    (setf (x (funge-space-offset funge-instance))
          (- (x (actual-cell-coords-beyond-cursor funge-instance))
             (- (tui:width (editor-window *app*)) 3)))
    (setf (x (cursor funge-instance))
          (- (tui:width (editor-window *app*)) 3)))
  (when (< (x (actual-cell-coords-beyond-cursor funge-instance))
           (x (funge-space-offset funge-instance)))
    (setf (x (funge-space-offset funge-instance))
          (x (actual-cell-coords-beyond-cursor funge-instance)))
    (setf (x (cursor funge-instance)) 0))
  (when (and (> (dimensions funge-instance) 1)
             (< (y (actual-cell-coords-beyond-cursor funge-instance))
                (y (funge-space-offset funge-instance))))
    (setf (y (funge-space-offset funge-instance))
          (y (actual-cell-coords-beyond-cursor funge-instance)))
    (setf (y (cursor funge-instance)) 0))
  (when (and (> (dimensions funge-instance) 1)
             (> (y (actual-cell-coords-beyond-cursor funge-instance))
                (+ (y (funge-space-offset funge-instance))
                   (- (tui:height (editor-window *app*)) 3))))
    (setf (y (funge-space-offset funge-instance))
          (- (y (actual-cell-coords-beyond-cursor funge-instance))
             (- (tui:height (editor-window *app*)) 3)))
    (setf (y (cursor funge-instance))
          (- (tui:height (editor-window *app*)) 3))))

;; stolen from http://stackoverflow.com/a/18680600
(defun %combinations (&rest lists)
  (if (car lists)
      (mapcan (lambda (inner-val)
                (mapcar (lambda (outer-val)
                          (cons outer-val
                                inner-val))
                        (car lists)))
              (apply #'%combinations (cdr lists)))
      (list nil)))

(defun %rotate-vectors (&rest vectors)
  (let ((veclen (length (first vectors)))
        (complete-list nil)
        (result nil))
    (loop for i in vectors
       do
         (setf complete-list (append complete-list i)))
    (loop for vn from 0 to (1- veclen)
       do (push (loop
                   for i = 0 then (incf i veclen)
                   for n = 0 then (incf n)
                   while (< n (length vectors))  
                   collect (nth (+ i vn) complete-list))
                result))
    (nreverse result)))

(defun calculate-significant-corners (some-corner1 some-corner2)
  "Normalizes any opposite corners of a funge-block into least/greatest
corners."
  (let ((possible-corners (apply #'%combinations
                                 (%rotate-vectors some-corner1 some-corner2))))
    (setf possible-corners
          (sort possible-corners #'< :key (lambda (x)
                                            (loop for i in x sum i))))
    (values (first possible-corners) (car (last possible-corners)))))

(defun insert-char-overwriting (key funge-instance)
  (push (list 'insert
              (actual-cell-coords-beyond-cursor
               (current-funge-instance *app*))
              (cell* (current-funge-instance *app*)
                     (actual-cell-coords-beyond-cursor
                      (current-funge-instance *app*))))
        (undo-list (current-funge-instance *app*)))
  (setf (cell* funge-instance
               (actual-cell-coords-beyond-cursor
                funge-instance))
        key))

(defun shift-funge-block (funge-instance corner1 corner2 move-delta)
  "Shifts an block, given by opposite corners to destination. Leaves
empty cells. Returns undo-instructions?"
  (multiple-value-bind (start end)  ; normalize corners to least-/greatest corners.
      (calculate-significant-corners corner1 corner2)
    (setf corner1 start)
    (setf corner2 end))
  (let ((temp-funge-space (make-instance 'funge-instance
                                         :dimensions (dimensions funge-instance))))
    (copy-funge-space funge-instance temp-funge-space ;; copy area into tmp-funge-space
                      :source-least-cell corner1
                      :source-greatest-cell corner2)
    (map-funge-space funge-instance ;; clear area
                     (lambda (vec)
                       (setf (cell* funge-instance vec)
                             32))
                     :least-corner corner1
                     :greatest-corner corner2)
    (copy-funge-space temp-funge-space funge-instance 
                      :target-offset (+-vector corner1
                                               move-delta ))
    (list 'multiple-instructions
          (list (list 'fill
                      (+-vector corner1 move-delta)
                      (+-vector corner2 move-delta)
                      32)
                (list 'insert-funge
                      corner1
                      temp-funge-space)))))

(defun insert-char-shifting (key funge-instance)
  (let* ((cursor-point (actual-cell-coords-beyond-cursor funge-instance))
         (start-point (loop          
                         for i-corners in (playfield-corners funge-instance)
                         for i-orientation-delta in (orientation-delta funge-instance)
                         for i-cursor in cursor-point
                         collect
                           (cond ((/= i-orientation-delta 0)
                                  i-cursor)
                                 ((= i-orientation-delta 0)
                                  (first i-corners)))))
         (end-point (loop          
                       for i-corners in (playfield-corners funge-instance)
                       for i-orientation-delta in (orientation-delta funge-instance)
                       collect
                         (cond ((> i-orientation-delta 0)
                                (second i-corners))
                               ((< i-orientation-delta 0)
                                (first i-corners))
                               ((= i-orientation-delta 0)
                                (second i-corners)))))
         (undo-instructions nil))
    (push (list 'insert  ; put cell under cursor on undo-stack
                (actual-cell-coords-beyond-cursor
                 (current-funge-instance *app*))
                (cell* (current-funge-instance *app*)
                       (actual-cell-coords-beyond-cursor
                        (current-funge-instance *app*))))
          undo-instructions)
    (push (shift-funge-block funge-instance
                             start-point
                             end-point
                             (orientation-delta funge-instance))
          undo-instructions)
    (setf (cell* (current-funge-instance *app*)
                 (actual-cell-coords-beyond-cursor
                  (current-funge-instance *app*)))
          key)
    (print-to-mini-buffer (format nil "start ~A end ~A dest ~A" start-point end-point (+-vector start-point (orientation-delta funge-instance))))
    (push (list 'multiple-instructions undo-instructions)
          (undo-list (current-funge-instance *app*)))))

(defun insert-char-inserting (key funge-instance)
  (let* ((start-point (actual-cell-coords-beyond-cursor funge-instance))
         (end-point        
          (loop          
             for i-corners in (playfield-corners funge-instance)
             for i-orientation-delta in (orientation-delta funge-instance)
             for i-start-point in start-point
             collect
               (cond ((> i-orientation-delta 0)
                      (second i-corners))
                     ((< i-orientation-delta 0)
                      (first i-corners))
                     (t
                      i-start-point))))
         (undo-instructions nil))
    (push (list 'insert
                (actual-cell-coords-beyond-cursor
                 (current-funge-instance *app*))
                (cell* (current-funge-instance *app*)
                       (actual-cell-coords-beyond-cursor
                        (current-funge-instance *app*))))
          undo-instructions)
    (push (shift-funge-block funge-instance
                             start-point
                             end-point
                             (orientation-delta funge-instance))
          undo-instructions)
    (setf (cell* (current-funge-instance *app*)
                 (actual-cell-coords-beyond-cursor
                  (current-funge-instance *app*)))
          key)
    (push (list 'multiple-instructions undo-instructions)
          (undo-list (current-funge-instance *app*)))))

;; (defun insert-char-inserting (key funge-instance)
;;   (let* ((start-point (actual-cell-coords-beyond-cursor funge-instance))
;;          (end-point        
;;           (loop          
;;              for i-corners in (playfield-corners funge-instance)
;;              for i-orientation-delta in (orientation-delta funge-instance)
;;              for i-start-point in start-point
;;              collect
;;                (cond ((> i-orientation-delta 0)
;;                       (second i-corners))
;;                      ((< i-orientation-delta 0)
;;                       (first i-corners))
;;                      (t
;;                       i-start-point))))
;;          (real-start-point start-point)
;;          (real-end-point end-point)
;;          (temp-funge-space (make-instance 'funge-instance
;;                                           :dimensions (dimensions funge-instance)))
;;          (undo-instructions nil))
;;     (push (list 'insert
;;                 (actual-cell-coords-beyond-cursor
;;                  (current-funge-instance *app*))
;;                 (cell* (current-funge-instance *app*)
;;                        (actual-cell-coords-beyond-cursor
;;                         (current-funge-instance *app*))))
;;           undo-instructions)
;;     (when (or (in-space-p funge-instance start-point) 
;;               (loop  
;;                  for i = start-point
;;                  then (setf start-point
;;                             (+-vector start-point
;;                                       (orientation-delta funge-instance)))
;;                  for n from 0 to 1000 ;; dirty...
;;                  do
;;                    (when (in-space-p funge-instance i)
;;                      (return t))))
;;       (when (> (reduce #'+ start-point)
;;                (reduce #'+ end-point))
;;         (let ((tmp start-point))
;;           (setf start-point end-point)
;;           (setf end-point tmp)))
;;       (let ((destination-point (+-vector start-point
;;                                          (orientation-delta funge-instance))))
;;         (copy-funge-space funge-instance temp-funge-space ;; copy area into tmp-funge-space
;;                           :source-least-cell start-point
;;                           :source-greatest-cell end-point)
;;         (map-funge-space funge-instance ;; clear area
;;                          (lambda (vec)
;;                            (setf (cell* funge-instance vec)
;;                                  32))
;;                          :least-corner start-point
;;                          :greatest-corner end-point)
;;         (push (list 'insert-funge   ; push to undo-list
;;                     start-point
;;                     temp-funge-space)
;;               undo-instructions)
;;         (let ((calc-start real-start-point)
;;               (calc-end (+-vector real-end-point
;;                                   (orientation-delta funge-instance))))
;;           (when (> (reduce #'+ calc-start)
;;                    (reduce #'+ calc-end))
;;             (let ((tmp start-point))
;;               (setf calc-start calc-end)
;;               (setf calc-end tmp)))
;;           (push (list 'fill calc-start
;;                       calc-end
;;                       32)
;;                 undo-instructions))
;;         (copy-funge-space temp-funge-space funge-instance ;; shifted backcopy from 
;;                           :target-offset destination-point) ;; tmp-funge-space
;;         (recalc-borders funge-instance)))
;;     (push (list 'multiple-instructions
;;                 undo-instructions)
;;           (undo-list (current-funge-instance *app*))))
;;   (setf (cell* (current-funge-instance *app*)
;;                (actual-cell-coords-beyond-cursor
;;                 (current-funge-instance *app*)))
;;         key))

(defun execute-undo-instruction (instruction)
  (cond
    ((equal (car instruction) 'insert)
     (setf (cell* (current-funge-instance *app*)
                  (second instruction))
           (third instruction))
     (unless (undo-list (current-funge-instance *app*))
       (setf (has-changed-p (current-funge-instance *app*))
             nil)))
    ((equal (car instruction) 'insert-funge)
     (copy-funge-space (third instruction)
                       (current-funge-instance *app*)
                       :target-offset (second instruction)))
    ((equal (car instruction) 'fill)
     (fill-funge-space (current-funge-instance *app*)
                       (fourth instruction)
                       :least-corner (second instruction)
                       :greatest-corner (third instruction)))
    ((equal (car instruction) 'multiple-instructions)
     (loop for iinstruction in (second instruction)
        do
          (execute-undo-instruction iinstruction)))))

(defun advance-by-orientation-delta (funge-instance)
  (if (> (dimensions funge-instance) 1)
      (setf (cursor funge-instance)
            (+-vector (cursor funge-instance)
                      (subseq (orientation-delta funge-instance) 0 2)))
      (setf (cursor funge-instance)
            (+-vector (cursor funge-instance)
                      (subseq (orientation-delta funge-instance) 0 1))))
  (when (> (dimensions funge-instance) 2)
    (loop for i from 2 to (1- (dimensions funge-instance))
       do
         (setf (nth i (funge-space-offset funge-instance))
               (+ (nth i (funge-space-offset funge-instance))
                  (nth i (orientation-delta funge-instance)))))))

(defun all-corners-of-cube (v1 v2)
  "Calculates all possible corners of an n-dimensional cube by two
given corners v1 and v2."
  (let ((corners
         (loop for i from 1 to (expt 2 (length v1))
            collect
              (make-n-list (length v1))))
        (n 0))
    (loop for skalar from 0 to (1- (length v1))
       do
         (loop for iexp1 from 1 to (expt 2 skalar)
            do
              (loop for i from 1 to (expt 2 (- (1- (length v1)) skalar))
                 do
                   (setf (nth skalar (nth n corners)) (nth skalar v1))
                   (incf n))
              (loop for i from 1 to (expt 2 (- (1- (length v1)) skalar))
                 do
                   (setf (nth skalar (nth n corners)) (nth skalar v2))
                   (incf n)))
         (setf n 0))
    corners))

(defun least-and-greates-corner-of-cube (list-of-corners)
  "Returns the least (min left/up for 2dim) and greatest (max
right/down for 2dim) corner of a n-dimensional cube."
  (let ((sortet-corners
         (sort
          list-of-corners
          #'<
          :key (lambda (vec)
                 (reduce #'+ vec)))))
    (list (first sortet-corners)
          (car (last sortet-corners)))))

(defparameter *mini-buffer-event-map*
  (copy-tree tui:*text-field-widget-keymap*))

(tui:addkey *mini-buffer-event-map* (#. (char-code #\Bel))
            leave-minibuffer-abort)

(tui:addkey *mini-buffer-event-map* (10)
            leave-minibuffer-success)

(defclass mini-buffer (tui:auto-stretch-text-field-widget)
  ((intro-text
    :initarg :intro-text
    :accessor intro-text
    :initform "")
   (stay-in-mini-buffer
    :initarg :stay-in-mini-buffer
    :accessor stay-in-mini-buffer
    :initform nil
    :documentation "If t, the mini-buffer stays the active window,
    otherwise the editor window gets activatet.")
   (tui::event-map
    :initform *mini-buffer-event-map*)))

(defmethod initialize-instance :before ((wi mini-buffer)
                                        &key intro-text window content)
  (setf (tui:x wi) (+ (length intro-text) 2))
  (setf (tui:y wi) 0)
  (setf (tui:dist-right wi) 0)
  (when content
    (setf (tui::x-pos-cursor wi) (length content)))
  (tui:wprint window 0 0 (format nil "~A: " intro-text)))

(defmethod tui:repaint ((wi mini-buffer) &key)
  (setf (tui:x wi) (+ (length (intro-text wi)) 2))
  (setf (tui:y wi) 0)
  (setf (tui:dist-right wi) 0)
  (tui:wprint (tui:mother-window wi) 0 0 (format nil "~A: " (intro-text wi)))
  (call-next-method))

(defun leave-minibuffer-abort (key)
  (declare (ignore key))
  (tui:deactivate-widget (tui:window :mini-buffer))
  (tui:destroy (tui:widget :mini-buffer :ask-via-minibuffer))
  (tui:clear (tui:window :mini-buffer))
  (tui:set-active (tui:window :editor)))

(defparameter *ask-via-minibuffer-func-on-success* nil)

(defun leave-minibuffer-success (key)
  (declare (ignore key))
  (let ((content (tui::content (tui:widget :mini-buffer :ask-via-minibuffer)))
        (stay-in-mini-buffer (stay-in-mini-buffer (tui:widget :mini-buffer :ask-via-minibuffer))))
    (tui:deactivate-widget (tui:window :mini-buffer))
    (tui:destroy (tui:widget :mini-buffer :ask-via-minibuffer))
    (tui:clear (tui:window :mini-buffer))
    (funcall *ask-via-minibuffer-func-on-success*
             content)
    (unless stay-in-mini-buffer
      (tui:set-active (tui:window :editor)))))

(eval-when (:execute :load-toplevel :compile-toplevel)
  (defun %ask-via-minibuffer (intro-text
                              &optional default-content
                                (widget-class 'mini-buffer)
                                completion-function
                                stay-in-mini-buffer)
    (tui:set-active (tui:window :mini-buffer))
    (make-instance widget-class
                   :window (tui:window :mini-buffer)
                   :name :ask-via-minibuffer
                   :content default-content
                   :intro-text intro-text
                   :completion-function completion-function
                   :stay-in-mini-buffer stay-in-mini-buffer)
    (tui:activate-widget (tui:window :mini-buffer)
                         (tui:widget :mini-buffer :ask-via-minibuffer))))

(defmacro ask-via-minibuffer ((return-var intro-text
                                          &key (default-content "")
                                          (widget-class (quote (quote mini-buffer)))
                                          (completion-function nil)
                                          (stay-in-mini-buffer nil))
                              &body body)
  `(progn
     (setf *ask-via-minibuffer-func-on-success*
           (lambda (,return-var)
             ,@body))
     (%ask-via-minibuffer ,intro-text ,default-content
                          ,widget-class ,completion-function ,stay-in-mini-buffer)))

(defun file-completion (string)
  (sort
   (mapcar
    #'namestring
    #+sbcl
    (remove-duplicates
     (append (directory
              (concatenate 'string string "*.*"))
             (directory
              (concatenate 'string string "*")))
     :key #'namestring
     :test #'string=)
    #+ccl
    (directory
     (concatenate 'string string "*.*"))
    #+clisp
    (nconc
     (directory
      (concatenate 'string string "*"))
     (directory
      (concatenate 'string string "*/")))
    #+ecl
    (nconc
     (directory (concatenate 'string
                             string
                             "*.*"))
     (directory (concatenate 'string
                             string
                             "*/"))))
   #'string<))

(defun find-common-string (list-of-strings)
  (let ((return-string nil))
    (if (> (length list-of-strings) 1)
        (loop for n = 0 then (incf n)
           while (loop for i in list-of-strings
                    always (and
                            (> (length i) n)
                            (equal (char i n)
                                   (char (first list-of-strings) n))))
           do
             (setf return-string
                   (concatenate 'string
                                return-string
                                (string
                                 (char (first list-of-strings)
                                       n)))))
        (setf return-string
              (first list-of-strings)))
    return-string))

(defparameter *ask-via-mini-buffer-event-map*
  (copy-tree *mini-buffer-event-map*))

(defclass mini-buffer-completions-list-window (tui:resizable-window
                                               tui:bordered-window
                                               tui:buffered-window)
  ())

(defclass popup-window (tui:resizable-window
                        tui:titled-window
                        ;; tui:buffered-window
                        tui::selector-window
                        tui:bordered-window)
  ((go-back-to-window
    :initarg :go-back-to-window
    :reader go-back-to-window
    :initform (error "Need a go-back-to-window!"))))

(tui:defkeymap *popup-window-event-map*
    (key)
  ((#. charms/ll:key_up) keyscroll-up
   (tui:scroll-up (tui:window :popup)))
  ((#. charms/ll:key_down) keyscroll-down
   (tui:scroll-down (tui:window :popup)))
  ((#. charms/ll:key_right) keyscroll-right
   (tui:scroll-right (tui:window :popup)))
  ((#. charms/ll:key_left) keyscroll-left
   (tui:scroll-left (tui:window :popup)))
  ((#. (char-code #\esc) #. (char-code #\<)) scroll-beginning
   (tui:scroll-up-to-begining (tui:window :popup)))
  ((#. (char-code #\esc) #. (char-code #\>)) scroll-ending
   (tui:scroll-down-to-end (tui:window :popup)))
  ((#. (char-code #\Soh)) scroll-left-begining
   (tui:scroll-to-left-begining (tui:window :popup)))
  ((#. (char-code #\Enq)) scroll-right-begining
   (tui:scroll-to-right-end (tui:window :popup)))
  ((#. (char-code #\Bel)) leave-popup-window
   (tui:set-active (go-back-to-window (tui:window :popup)))
   (tui:kill (tui:window :popup))
   (tui:repaint tui::*screen*)
   (tui:do-update))
  ((or ((32 126))
       (#. (char-code #\Newline))
       (#. charms/ll:key_backspace)
       (#. (char-code #\Stx))
       (#. (char-code #\Ack))) leave-popup-window-keystroke
   (tui:set-active (go-back-to-window (tui:window :popup)))
   (tui:kill (tui:window :popup))
   (tui:repaint tui::*screen*)
   (tui:do-update)
   (charms/ll:ungetch key)))

(defun popup-window (text-list go-back-to-window)
  (make-instance 'popup-window
                 :name :popup
                 :title "Completions"
                 :start-left-dist 5
                 :start-bottom-dist 25
                 :end-right-dist 5
                 :end-bottom-dist 1
                 :draw-func nil
                 :go-back-to-window go-back-to-window
                 :event-map *popup-window-event-map*)
  (loop
     for itext in text-list
     for n = 1 then (incf n)
     do
       (tui:wprint (tui:window :popup) 2 n itext))
  (tui:set-active (tui:window :popup)))

(defun mini-buffer-file-completion (key)
  (declare (ignore key))
  (when (completion-function
         (tui:widget :mini-buffer :ask-via-minibuffer))
    (let ((completion-list (if (completion-function
                                (tui:widget :mini-buffer :ask-via-minibuffer))
                               (funcall (completion-function
                                         (tui:widget :mini-buffer :ask-via-minibuffer))
                                        (tui::content
                                         (tui:widget :mini-buffer :ask-via-minibuffer)))
                               nil)))
      (when completion-list
        (let ((common-string (find-common-string completion-list)))
          (if (= (length (tui::content
                          (tui:widget :mini-buffer :ask-via-minibuffer)))
                 (length common-string))
              (progn
                (popup-window completion-list (tui:window :mini-buffer)))
              (progn
                (setf
                 (tui::content
                  (tui:widget :mini-buffer :ask-via-minibuffer))
                 common-string)
                (setf
                 (tui::x-pos-cursor
                  (tui:widget :mini-buffer :ask-via-minibuffer))
                 (length common-string)))))))))

(tui:addkey *mini-buffer-event-map* (#. (char-code #\Tab))
            mini-buffer-file-completion)

(defclass mini-buffer-file-select (mini-buffer)
  ((tui::event-map
    :initform *mini-buffer-event-map*)
   (completion-function
    :initarg :completion-function
    :reader completion-function
    :initform nil)))

(defun name-from-full-filepath (filepath)
  (subseq filepath
          (1+ (search "/" filepath :from-end t))))

(defun find-funge-instance ()
  (ask-via-minibuffer (filename "Find file"
                                :default-content "~/"
                                :widget-class 'mini-buffer-file-select
                                :completion-function 'file-completion
                                :stay-in-mini-buffer t)
    (load/create-window
     filename
     (lambda (filename props dimensions)
       (if (fad:directory-exists-p filename)
           (print-to-mini-buffer
            (format nil "\"~A\" is a directory." filename))
           (progn
             (let ((new-funge-instance
                    (make-instance 'funge-instance-in-editor
                                   :full-file-name filename
                                   :name (name-from-full-filepath filename)
                                   :version props
                                   :dimensions dimensions)))
               (if (probe-file filename)
                   (progn
                     (read-funge-native new-funge-instance filename)
                     (print-to-mini-buffer "Funge-instance loaded."))
                   (progn
                     (print-to-mini-buffer "Funge-instance created.")))
               (push new-funge-instance (funge-instances *app*)))))))))

(defun set-current-funge-directory ()
  (ask-via-minibuffer (directory-name "Set current directory"
                                      :default-content (namestring
                                                        (current-directory
                                                         (current-funge-instance *app*)))
                                      :widget-class 'mini-buffer-file-select
                                      :completion-function 'file-completion
                                      :stay-in-mini-buffer nil)
    (setf directory-name (pathname directory-name))
    (if (fad:directory-exists-p directory-name)
        (setf (current-directory (current-funge-instance *app*))
              directory-name)
        (print-to-mini-buffer
         (format nil "~A is no valid directory."
                 directory-name)))))

(defun write-funge-instance ()
  (ask-via-minibuffer (filename "Write funge instance"
                                :default-content "~/"
                                :widget-class 'mini-buffer-file-select
                                :completion-function 'file-completion
                                :stay-in-mini-buffer nil)
    (setf (full-file-name (current-funge-instance *app*))
          filename)
    (setf (name (current-funge-instance *app*))
          (name-from-full-filepath filename))
    (write-funge-native (current-funge-instance *app*)
                        (full-file-name (current-funge-instance *app*))
                        (make-n-list (dimensions (current-funge-instance *app*)))
                        (greatest-coord (current-funge-instance *app*)))
    (setf (has-changed-p (current-funge-instance *app*)) nil)
    (print-to-mini-buffer "Funge-instance written.")))

(defun buffer-completion (string)
  (sort
   (loop for i in (mapcar #'name
                          (funge-instances *app*))
      when (search string i)
      collect i)
   #'string<))

(defun switch-funge-instance ()
  (ask-via-minibuffer (buffername "Switch instance"
                                  :default-content ""
                                  :widget-class 'mini-buffer-file-select
                                  :completion-function 'buffer-completion)
    (aif (find buffername (funge-instances *app*) :test #'string= :key #'name)
         (progn
           (setf (funge-instances *app*)
                 (cons it
                       (remove it (funge-instances *app*) :test #'equal)))
           (tui:repaint (tui:window :thread)))
         (print-to-mini-buffer (format nil
                                       "There is no such funge-instance named ~A."
                                       buffername)))))

(defun kill-funge-instance ()
  (ask-via-minibuffer (buffername "Kill instance"
                                  :default-content (name (current-funge-instance *app*))
                                  :widget-class 'mini-buffer-file-select
                                  :completion-function 'buffer-completion)
    (aif (find buffername (funge-instances *app*) :test #'string= :key #'name)
         (progn
           (funge:kill-funge-space it)
           (setf (funge-instances *app*)
                 (remove it (funge-instances *app*) :test #'equal))
           (unless (funge-instances *app*)
             (push (make-instance 'funge-instance-in-editor)
                   (funge-instances *app*))))
         (print-to-mini-buffer (format nil
                                       "There is no such funge-instance named ~A."
                                       buffername)))))

(defun kill-region (funge-space)
  (let* ((corners
          (least-and-greates-corner-of-cube
           (all-corners-of-cube
            (mark funge-space)
            (actual-cell-coords-beyond-cursor funge-space))))
         (c1 (first corners))
         (c2 (second corners)))
    (map-funge-space funge-space
                     (lambda (vec)
                       (setf (cell* funge-space vec) 32))
                     :least-corner c1
                     :greatest-corner c2)))

(defun push-region-to-clipboard (funge-space)
  (let* ((corners
          (least-and-greates-corner-of-cube
           (all-corners-of-cube
            (mark funge-space)
            (actual-cell-coords-beyond-cursor funge-space))))
         (c1 (first corners))
         (c2 (second corners))
         (clipboard-funge
          (make-instance 'funge-space :dimensions (dimensions funge-space))))
    (copy-funge-space funge-space clipboard-funge
                      :source-least-cell c1
                      :source-greatest-cell c2)
    (push clipboard-funge (clipboard *app*))))

(defun push-clipboard-to-funge (funge-space &optional (pos-in-killring 0))
  (let ((clipboard-funge (nth pos-in-killring (clipboard *app*))))
    (copy-funge-space clipboard-funge
                      (current-funge-instance *app*)
                      :target-offset (actual-cell-coords-beyond-cursor
                                      funge-space))))

(defun save-funge-instance ()
  (if (full-file-name (current-funge-instance *app*))
      (progn
        (write-funge-native (current-funge-instance *app*)
                            (full-file-name (current-funge-instance *app*))
                            (make-n-list (dimensions (current-funge-instance *app*)))
                            (greatest-coord (current-funge-instance *app*)))
        (setf (has-changed-p (current-funge-instance *app*)) nil)
        (print-to-mini-buffer "Funge-instance saved."))
      (write-funge-instance)))

(defun reset-ip ()
  (setf (threads (current-funge-instance *app*))
        nil)
  (push (make-instance 'thread
                       :parent-funge-instance (current-funge-instance *app*))
        (threads (current-funge-instance *app*)))
  (setf (thread-counter (current-funge-instance *app*)) 0)
  (setf (no-of-ticks (current-funge-instance *app*)) 0))

(defun search-funge-space (fs search-string search-delta &optional (start-least-corner (least-coord fs)))
  "Searches for SEARCH-STRING in FS in SEARCH-DELTA direction and
  returns list of starting coordinates of SEARCH-STRING."
  (let ((found nil))
    (map-funge-space
     fs
     (lambda (vec)
       (loop
          named search-loop
          for i across search-string
          for pos = vec then (setf pos
                                   (+-vector pos search-delta))
          unless (char= i (code-char (cell* fs pos)))
          do
            (return-from search-loop)
          finally (push vec found)))
     :least-corner start-least-corner)
    found))

(defclass load/create-funge-window (tui:titled-window
                                    tui:centered-window
                                    tui:bordered-window
                                    tui:widgeted-window)
  ((func-execute
    :initarg :func-execute
    :accessor func-execute
    :initform nil
    :documentation "function to execute after load/create
    window. Expects as parameters: filename, a list of keywords
    characterizing the funge-instance and the dimensions.")
   (filename
    :initarg :filename
    :reader filename)))

(defparameter *debug* nil)

(tui:defkeymap *load/create-window-event-map*
    (key)
  ((#. (char-code #\Newline)) success-load/create-funge-exit-window
   (let ((w (tui:active-window tui:*screen*))
         funge-properties)
     (when (tui:selected-p (tui:widget :load/create-funge-window
                                       :select-box-befunge93))
       (push :befunge93 funge-properties))
     (when (tui:selected-p (tui:widget :load/create-funge-window
                                       :select-box-befunge98))
       (push :befunge98 funge-properties))
     (when (tui:selected-p (tui:widget :load/create-funge-window
                                       :select-box-unefunge98))
       (push :unefunge98 funge-properties))
     (when (tui:selected-p (tui:widget :load/create-funge-window
                                       :select-box-trefunge))
       (push :trefunge98 funge-properties))
     (when (tui:selected-p (tui:widget :load/create-funge-window
                                       :select-box-concurrent-funge))
       (push :concurrent funge-properties))
     (when (tui:selected-p (tui:widget :load/create-funge-window
                                       :select-box-filesystem-funge))
       (push :filesystem funge-properties))
     (when (tui:selected-p (tui:widget :load/create-funge-window
                                       :select-box-system-execution))
       (push :system-execution funge-properties))
     (funcall (func-execute w)
              (filename w)
              funge-properties
              (parse-integer (tui:content (tui:widget :load/create-funge-window
                                                      :text-field-dimensions))))
     (tui:set-active (tui:window :editor))
     (tui:kill w)
     (tui:repaint tui:*screen*)))
  ((#. (char-code #\Bel)) cancel-load/create-funge-exit-window
   (let ((w (tui:active-window tui:*screen*)))
     (tui:set-active (tui:window :editor))
     (tui:kill w)
     (tui:repaint tui:*screen*))))

(defun load/create-window (filename func)
  (make-instance 'load/create-funge-window
                 :name :load/create-funge-window
                 :title "Create/Load funge-instance..."
                 :width 76
                 :height 16
                 :draw-func (lambda (self)
                              (tui:wprint self 2 2 (format nil "File: ~A"
                                                           filename) )
                              (tui:wprint self 7 4 "Load funge instruction-sets: " )
                              (tui:wprint self 7 11 "Dimensions: "))
                 :event-map *load/create-window-event-map*
                 :func-execute func
                 :filename filename)
  (make-instance 'tui:radio-button :window (tui:window :load/create-funge-window)
                 :name :select-box-befunge93
                 :x 17 :y 6
                 :content "Befunge-93"
                 :func-to-call-on-act
                 (lambda (self)
                   (declare (ignore self))
                   (setf (tui:content (tui:widget :load/create-funge-window
                                                  :text-field-dimensions))
                         "2"))
                 :add-event-map *load/create-window-event-map*)
  (make-instance 'tui:radio-button :window (tui:window :load/create-funge-window)
                 :name :select-box-unefunge98
                 :x 17 :y 7
                 :content "Unefunge-98"
                 :func-to-call-on-act
                 (lambda (self)
                   (declare (ignore self))
                   (setf (tui:content (tui:widget :load/create-funge-window
                                                  :text-field-dimensions))
                         "1"))
                 :add-event-map *load/create-window-event-map*)
  (make-instance 'tui:radio-button :window (tui:window :load/create-funge-window)
                 :name :select-box-befunge98
                 :x 17 :y 8
                 :content "Befunge-98"
                 :selected-p nil
                 :func-to-call-on-act
                 (lambda (self)
                   (declare (ignore self))
                   (setf (tui:content (tui:widget :load/create-funge-window
                                                  :text-field-dimensions))
                         "2"))
                 :add-event-map *load/create-window-event-map*)
  (make-instance 'tui:radio-button :window (tui:window :load/create-funge-window)
                 :name :select-box-trefunge
                 :x 17 :y 9
                 :content "Trefunge-98"
                 :func-to-call-on-act
                 (lambda (self)
                   (declare (ignore self))
                   (setf (tui:content (tui:widget :load/create-funge-window
                                                  :text-field-dimensions))
                         "3"))
                 :add-event-map *load/create-window-event-map*)
  (make-instance 'tui:select-box :window (tui:window :load/create-funge-window)
                 :name :select-box-concurrent-funge
                 :x 38 :y 7
                 :content "Concurrent Funge"
                 :selected-p nil
                 :add-event-map *load/create-window-event-map*)
  (make-instance 'tui:select-box :window (tui:window :load/create-funge-window)
                 :name :select-box-filesystem-funge
                 :x 38 :y 8
                 :content "Filesystem Funge"
                 :selected-p nil
                 :add-event-map *load/create-window-event-map*)
  (make-instance 'tui:select-box :window (tui:window :load/create-funge-window)
                 :name :select-box-system-execution
                 :x 38 :y 9
                 :content "System Execution"
                 :selected-p nil
                 :add-event-map *load/create-window-event-map*)
  (make-instance 'tui:text-field :window (tui:window :load/create-funge-window)
                 :name :text-field-dimensions
                 :x 19 :y 11
                 :width 3
                 :max-content-length 3
                 :content "2"
                 :validate-regex "^[0-9]*$"
                 :add-event-map *load/create-window-event-map*)
  (make-instance 'tui:button :window (tui:window :load/create-funge-window)
                 :name :button-cancel
                 :x 12 :y 13
                 :content "Cancel (Ctrl-g)"
                 :func-to-call-on-act nil
                 ;; (lambda (self)
                 ;;   (tui:set-active (tui:window :editor))
                 ;;   (tui:kill (tui:mother-window self))
                 ;;   (tui:repaint tui:*screen*))
                 :add-event-map *load/create-window-event-map*)
  (make-instance 'tui:button :window (tui:window :load/create-funge-window)
                 :name :button-ok
                 :x 34 :y 13
                 :content "Ok (Return)"
                 :func-to-call-on-act nil
                 ;; (lambda (self)
                 ;;   (tui:set-active (tui:window :editor))
                 ;;   (tui:kill (tui:mother-window self)))
                 :add-event-map *load/create-window-event-map*)
  (cond ((ppcre:scan ".*\\.bf$" filename)
         ;; befunge-93 - without filesystem and concurrent
         (setf (tui:selected-p (tui:widget :load/create-funge-window
                                           :select-box-befunge93))
               t)
         (setf (tui:content (tui:widget :load/create-funge-window
                                        :text-field-dimensions))
               "2"))
        ((ppcre:scan ".*\\.b98" filename)
         ;; befunge-98 - defaults to with filesystem and concurrent
         (setf (tui:selected-p (tui:widget :load/create-funge-window
                                           :select-box-befunge98))
               t)
         (setf (tui:selected-p (tui:widget :load/create-funge-window
                                           :select-box-concurrent-funge))
               t)
         (setf (tui:selected-p (tui:widget :load/create-funge-window
                                           :select-box-filesystem-funge))
               t)
         (setf (tui:content (tui:widget :load/create-funge-window
                                        :text-field-dimensions))
               "2"))
        ((ppcre:scan ".*\\.u98" filename)
         ;; unefunge-98 - defaults to with filesystem and concurrent
         (setf (tui:selected-p (tui:widget :load/create-funge-window
                                           :select-box-unefunge98))
               t)
         (setf (tui:selected-p (tui:widget :load/create-funge-window
                                           :select-box-concurrent-funge))
               t)
         (setf (tui:selected-p (tui:widget :load/create-funge-window
                                           :select-box-filesystem-funge))
               t)
         (setf (tui:content (tui:widget :load/create-funge-window
                                        :text-field-dimensions))
               "1"))
        ((ppcre:scan ".*\\.t98" filename)
         ;; trefunge-98 - defaults to with filesystem and concurrent
         (setf (tui:selected-p (tui:widget :load/create-funge-window
                                           :select-box-trefunge))
               t)
         (setf (tui:selected-p (tui:widget :load/create-funge-window
                                           :select-box-concurrent-funge))
               t)
         (setf (tui:selected-p (tui:widget :load/create-funge-window
                                           :select-box-filesystem-funge))
               t)
         (setf (tui:content (tui:widget :load/create-funge-window
                                        :text-field-dimensions))
               "3"))
        (t
         ;; ? defaults to befunge-98
         (setf (tui:selected-p (tui:widget :load/create-funge-window
                                           :select-box-befunge98))
               t)
         (setf (tui:selected-p (tui:widget :load/create-funge-window
                                           :select-box-concurrent-funge))
               t)
         (setf (tui:selected-p (tui:widget :load/create-funge-window
                                           :select-box-filesystem-funge))
               t)
         (setf (tui:content (tui:widget :load/create-funge-window
                                        :text-field-dimensions))
               "2")))
  (tui:set-active (tui:window :load/create-funge-window))
  (tui:repaint tui:*screen*)
  (tui:activate-widget (tui:window :load/create-funge-window)
                       (tui:widget :load/create-funge-window
                                   :select-box-befunge93)))
