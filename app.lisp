(in-package :toadstool)

(declaim (optimize (debug 3) (speed 0)))

(defvar *swank-server* nil)

(defclass app ()
  ((editor-window
    :initarg :editor-window
    :accessor editor-window)
   (thread-window
    :initarg :thread-window
    :accessor thread-window)
   (output-window
    :initarg :output-window
    :accessor output-window)
   (mini-buffer-window
    :initarg :mini-buffer-window
    :accessor mini-buffer-window)
   (funge-instances
    :initarg :funge-instances
    :accessor funge-instances
    :initform nil)
   (clipboard
    :initarg :clipboard
    :accessor clipboard
    :initform nil)
   (clear-mini-buffer
    :accessor clear-mini-buffer
    :initform nil)
   (previous-command
    :accessor previous-command
    :initform nil)
   (previous-command-issued-p
    :accessor previous-command-issued-p
    :initform nil)))

(defmethod current-funge-instance ((app app))
  (car (funge-instances app)))

(defclass editor-window (tui:resizable-window
                         tui:titled-window
                         tui:widgeted-window
                         tui:bordered-window)
  ())

(defclass thread-window (tui:resizable-window
                         tui:titled-window
                         tui:buffered-window
                         tui:bordered-window)
  ())

(defclass output-window (tui:resizable-window
                         tui:titled-window
                         tui:buffered-window
                         tui:bordered-window)
  ())

(defclass mini-buffer-window (tui:resizable-window tui:widgeted-window)
  ())

(defvar *app* nil)

(defvar *output-line-x* nil)
(defvar *output-line-y* nil)

(defvar *running-funge-instance-toggle-debug* t)
(defvar *running-funge-instance-execution-delay* 0)

(defparameter *toggle-zoom-editor-window* nil)

(define-condition exit-running-funge-instance ()
  ())

(defun send-standard-output-to-output-window ()
  (let ((fetched-output (get-output-stream-string *standard-output*)))
    (when (> (length fetched-output) 0)
      (loop for i across fetched-output
         do
           (tui:wput (tui:window :output)
                     *output-line-x* *output-line-y*
                     i)
           (incf *output-line-x*)
           (when (or (char-equal i #\Newline)
                     (char-equal i #\Return))
             (incf *output-line-y*)
             (setf *output-line-x* 1)))
      (tui::scroll-down-to-end (tui:window :output))
      (tui:repaint (tui:window :output)))))

(defun parse-input-vector (string)
  (let ((regex-string-parens (format
                              nil
                              "^\\(~{~A~^ ~}\\)$"
                              (loop
                                 for i
                                 from 1
                                 to (dimensions (current-funge-instance *app*))
                                 collect "-*[0-9]+")))
        (regex-string-spaces (format
                              nil
                              "^~{~A~^ ~}$"
                              (loop
                                 for i
                                 from 1
                                 to (dimensions (current-funge-instance *app*))
                                 collect "-*[0-9]+"))))
    (cond
      ((cl-ppcre:scan regex-string-spaces string)
       (read-from-string (concatenate 'string "(" string ")")))
      ((cl-ppcre:scan regex-string-parens string)
       (read-from-string string))
      (t nil))))

(tui:defkeymap *editor-event-map*
    (key)
  ;; input
  (((32 126)) insert-char
              (setf (has-changed-p (current-funge-instance *app*)) t)
              (setf (mark-aktive-p (current-funge-instance *app*))
                    nil
                    (search-result (current-funge-instance *app*))
                    nil)
              (cond ((equal (insert-mode (current-funge-instance *app*)) :overwrite)
                     (insert-char-overwriting key (current-funge-instance *app*)))
                    ((equal (insert-mode (current-funge-instance *app*)) :insert)
                     (insert-char-inserting key (current-funge-instance *app*)))
                    ((equal (insert-mode (current-funge-instance *app*)) :shift)
                     (insert-char-shifting key (current-funge-instance *app*))))
              (advance-by-orientation-delta (current-funge-instance *app*))
              (handle-cursor-if-out-of-view (current-funge-instance *app*)))
  ;; ^d
  ((#.(char-code #\Eot)) strg-d
                         (cond
                           ((equal (insert-mode (current-funge-instance *app*)) :overwrite)
                            (push (list 'insert
                                        (actual-cell-coords-beyond-cursor
                                         (current-funge-instance *app*))
                                        (cell* (current-funge-instance *app*)
                                               (actual-cell-coords-beyond-cursor
                                                (current-funge-instance *app*))))
                                  (undo-list (current-funge-instance *app*)))
                            (setf (cell* (current-funge-instance *app*)
                                         (actual-cell-coords-beyond-cursor
                                          (current-funge-instance *app*)))
                                  32))
                           ((equal (insert-mode (current-funge-instance *app*)) :insert)
                            (let* ((start-point
                                     (+-vector (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
                                               (orientation-delta (current-funge-instance *app*))))
                                   (end-point        
                                     (loop          
                                       for i-corners in (playfield-corners (current-funge-instance *app*))
                                       for i-orientation-delta in (orientation-delta (current-funge-instance *app*))
                                       for i-start-point in start-point
                                       collect
                                       (cond ((> i-orientation-delta 0)
                                              (second i-corners))
                                             ((< i-orientation-delta 0)
                                              (first i-corners))
                                             (t
                                              i-start-point))))
                                   (temp-funge-space (make-instance 'funge-instance
                                                                    :dimensions (dimensions (current-funge-instance *app*)))))
                              (multiple-value-bind (start-point end-point)
                                  (calculate-significant-corners (+-vector start-point
                                                                           (*-vector-skalar (orientation-delta (current-funge-instance *app*))
                                                                                            -1))
                                                                 end-point)
                                (copy-funge-space (current-funge-instance *app*) temp-funge-space
                                                  :source-least-cell start-point
                                                  :source-greatest-cell end-point)
                                (push (list 'insert-funge
                                            start-point
                                            temp-funge-space)
                                      (undo-list (current-funge-instance *app*))))
                              (shift-funge-block (current-funge-instance *app*)
                                                 start-point
                                                 end-point
                                                 (*-vector-skalar (orientation-delta (current-funge-instance *app*))
                                                                  -1))))
                           ((equal (insert-mode (current-funge-instance *app*)) :shift)
                            (let* ((cursor-point
                                     (+-vector (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
                                               (orientation-delta (current-funge-instance *app*))))
                                   (start-point
                                     (loop          
                                       for i-corners in (playfield-corners (current-funge-instance *app*))
                                       for i-orientation-delta in (orientation-delta (current-funge-instance *app*))
                                       for i-cursor in cursor-point
                                       collect
                                       (cond ((/= i-orientation-delta 0)
                                              i-cursor)
                                             ((= i-orientation-delta 0)
                                              (first i-corners)))))
                                   (end-point
                                     (loop          
                                       for i-corners in (playfield-corners (current-funge-instance *app*))
                                       for i-orientation-delta in (orientation-delta (current-funge-instance *app*))
                                       collect
                                       (cond ((> i-orientation-delta 0)
                                              (second i-corners))
                                             ((< i-orientation-delta 0)
                                              (first i-corners))
                                             ((= i-orientation-delta 0)
                                              (second i-corners)))))
                                   (temp-funge-space (make-instance 'funge-instance
                                                                    :dimensions (dimensions (current-funge-instance *app*)))))
                              (multiple-value-bind (start-point end-point)
                                  (calculate-significant-corners (+-vector start-point
                                                                           (*-vector-skalar (orientation-delta (current-funge-instance *app*))
                                                                                            -1))
                                                                 end-point)
                                (copy-funge-space (current-funge-instance *app*) temp-funge-space
                                                  :source-least-cell start-point
                                                  :source-greatest-cell end-point)
                                (push (list 'insert-funge
                                            start-point
                                            temp-funge-space)
                                      (undo-list (current-funge-instance *app*))))
                              (shift-funge-block (current-funge-instance *app*)
                                                 start-point
                                                 end-point
                                                 (*-vector-skalar (orientation-delta (current-funge-instance *app*))
                                                                  -1 )))))
                         
                         (handle-cursor-if-out-of-view (current-funge-instance *app*))   )
  ;; show cell value
  ((24 #.(char-code #\=)) show-cell-value
                          (print-to-mini-buffer
                           (format nil "Cell value: ~d, #x~x, #o~o"
                                   (actual-cell-beyond-cursor (current-funge-instance *app*))
                                   (actual-cell-beyond-cursor (current-funge-instance *app*))
                                   (actual-cell-beyond-cursor (current-funge-instance *app*)))))
  ((24 #. (char-code #\+)) increase-editor-window-hor
                           (when (and (not *toggle-zoom-editor-window*)
                                      (> (tui:height (tui:window :output)) 3))
                             (decf (tui::end-bottom-dist (tui:window :editor)))
                             (decf (tui::end-bottom-dist (tui:window :thread)))
                             (decf (tui::start-bottom-dist (tui:window :output)))
                             (tui:repaint tui:*screen*)))
  ((24 #. (char-code #\-)) decrease-editor-window-hor
                           (when (and (not *toggle-zoom-editor-window*)
                                      (> (tui:height (tui:window :editor)) 3))
                             (incf (tui::end-bottom-dist (tui:window :editor)))
                             (incf (tui::end-bottom-dist (tui:window :thread)))
                             (incf (tui::start-bottom-dist (tui:window :output)))
                             (tui:repaint tui:*screen*)))
  ((24 #. (char-code #\{)) decrease-editor-window-ver
                           (when (and (not *toggle-zoom-editor-window*)
                                      (> (tui:width (tui:window :editor)) 5))
                             (incf (tui::end-right-dist (tui:window :editor)))
                             (incf (tui::start-right-dist (tui:window :thread)))
                             (tui:repaint tui:*screen*)))
  ((24 #. (char-code #\})) increase-editor-window-ver
                           (when (and
                                  (not *toggle-zoom-editor-window*)
                                  (> (tui:width (tui:window :thread)) 5))
                             (decf (tui::end-right-dist (tui:window :editor)))
                             (decf (tui::start-right-dist (tui:window :thread)))
                             (tui:repaint tui:*screen*)))
  ((24 #. (char-code #\z)) zoom-editor-window
                           (if *toggle-zoom-editor-window*
                               (setf (tui::end-bottom-dist (tui:window :editor))
                                     (first *toggle-zoom-editor-window*)
                                     (tui::end-right-dist (tui:window :editor))
                                     (second *toggle-zoom-editor-window*)
                                     *toggle-zoom-editor-window* nil)
                               (setf *toggle-zoom-editor-window*
                                     (list
                                      (tui::end-bottom-dist (tui:window :editor))
                                      (tui::end-right-dist (tui:window :editor)))
                                     (tui::end-bottom-dist (tui:window :editor)) 1
                                     (tui::end-right-dist (tui:window :editor)) 0))
                           (tui:repaint tui:*screen*))
  ;; mark/kill/yank
  ((#.(char-code #\Nul)) set-mark
                         (setf (mark (current-funge-instance *app*))
                               (actual-cell-coords-beyond-cursor
                                (current-funge-instance *app*)))
                         (setf (mark-aktive-p (current-funge-instance *app*))
                               t))
  ((#. (char-code #\Bel)) break
                          (setf (mark-aktive-p (current-funge-instance *app*))
                                nil
                                (search-result (current-funge-instance *app*))
                                nil))
  ((#.(char-code #\esc) #.(char-code #\w)) copy-region-to-clipboard
                                           (when (mark (current-funge-instance *app*))
                                             (push-region-to-clipboard (current-funge-instance *app*))
                                             (setf (mark-aktive-p (current-funge-instance *app*)) nil)))
  ((#.(char-code #\Etb)) kill-region
                         (when (mark (current-funge-instance *app*))
                           (push-region-to-clipboard (current-funge-instance *app*))
                           (let* ((funge-to-save
                                    (make-instance 'funge-space
                                                   :dimensions (dimensions
                                                                (current-funge-instance *app*))))
                                  (corners
                                    (least-and-greates-corner-of-cube
                                     (all-corners-of-cube
                                      (mark (current-funge-instance *app*))
                                      (actual-cell-coords-beyond-cursor (current-funge-instance *app*)))))
                                  (c1 (first corners))
                                  (c2 (second corners)))
                             (copy-funge-space (current-funge-instance *app*)
                                               funge-to-save
                                               :source-least-cell c1
                                               :source-greatest-cell c2)
                             (push (list 'insert-funge
                                         c1
                                         funge-to-save)
                                   (undo-list (current-funge-instance *app*))))
                           (kill-region (current-funge-instance *app*))
                           (setf (mark-aktive-p (current-funge-instance *app*)) nil)))
  ((#.(char-code #\Em)) yank
                        (when (clipboard *app*) 
                          (setf (has-changed-p (current-funge-instance *app*)) t)
                          (let ((funge-to-save
                                  (make-instance 'funge-space
                                                 :dimensions (dimensions
                                                              (current-funge-instance *app*))))
                                (funge-to-be-pasted (car (clipboard *app*))))
                            (copy-funge-space (current-funge-instance *app*)
                                              funge-to-save
                                              :source-least-cell
                                              (actual-cell-coords-beyond-cursor
                                               (current-funge-instance *app*))
                                              :source-greatest-cell
                                              (+-vector (actual-cell-coords-beyond-cursor
                                                         (current-funge-instance *app*))
                                                        (--vector
                                                         (greatest-coord funge-to-be-pasted)
                                                         (least-coord funge-to-be-pasted))))
                            (push (list 'insert-funge
                                        (actual-cell-coords-beyond-cursor
                                         (current-funge-instance *app*))
                                        funge-to-save)
                                  (undo-list (current-funge-instance *app*))))
                          (push-clipboard-to-funge (current-funge-instance *app*))
                          (setf (previous-command *app*) (list 'yank 0))
                          (setf (previous-command-issued-p *app*) t))) 
  ((#. (char-code #\esc) #. (char-code #\y)) yank-next-from-killring
                                             (if (equal (car (previous-command *app*)) 'yank)
                                                 (progn
                                                   (let ((undo-instruction
                                                           (pop (undo-list (current-funge-instance *app*)))))
                                                     (incf (second (previous-command *app*)))
                                                     (copy-funge-space (third undo-instruction)
                                                                       (current-funge-instance *app*)
                                                                       :target-offset (second undo-instruction))
                                                     (let ((funge-to-save
                                                             (make-instance
                                                              'funge-space
                                                              :dimensions (dimensions (current-funge-instance
                                                                                       *app*))))
                                                           (funge-to-be-pasted (nth (mod
                                                                                     (second
                                                                                      (previous-command *app*))
                                                                                     (length (clipboard *app*)))
                                                                                    (clipboard *app*))))
                                                       (copy-funge-space (current-funge-instance *app*)
                                                                         funge-to-save
                                                                         :source-least-cell
                                                                         (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
                                                                         :source-greatest-cell
                                                                         (+-vector
                                                                          (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
                                                                          (--vector
                                                                           (greatest-coord funge-to-be-pasted)
                                                                           (least-coord funge-to-be-pasted))))
                                                       (push (list 'insert-funge
                                                                   (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
                                                                   funge-to-save)
                                                             (undo-list (current-funge-instance *app*))))
                                                     (push-clipboard-to-funge
                                                      (current-funge-instance *app*)
                                                      (mod (second (previous-command *app*))
                                                           (length (clipboard *app*))))
                                                     (setf (previous-command-issued-p *app*) t)))
                                                 (print-to-mini-buffer "Previous command was not a yank.")))
  ;; search
  ((#.(char-code #\Dc3)) search
                         (aif (search-result (current-funge-instance *app*))
                              (progn
                                (let ((new-cursor-position (elt it
                                                                (search-result-position
                                                                 (current-funge-instance *app*)))))
                                  (setf (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
                                        new-cursor-position)
                                  (handle-cursor-if-out-of-view (current-funge-instance *app*))
                                  (incf (search-result-position
                                         (current-funge-instance *app*)))
                                  (if (< (search-result-position
                                          (current-funge-instance *app*))
                                         (length it))
                                      (print-to-mini-buffer
                                       (format
                                        nil "~A/~A occurcence. Press <Ctrl-s> to jump to next occurence. <Ctrl-g> cancels search."
                                        (search-result-position
                                         (current-funge-instance *app*))
                                        (length
                                         it)))
                                      (progn
                                        (setf (search-result-position
                                               (current-funge-instance *app*))
                                              0)
                                        (print-to-mini-buffer
                                         (format
                                          nil "~A/~A (last) occurence. <Ctrl-s> to jump to first occurence. <Ctrl-g> cancels search."
                                          (length
                                           it)
                                          (length
                                           it) ))))))
                              (progn
                                (setf (search-result-position
                                       (current-funge-instance *app*))
                                      0)
                                (ask-via-minibuffer (search-pattern "Search forward"
                                                     :widget-class 'mini-buffer-file-select)
                                  (let ((found (reverse
                                                (search-funge-space (current-funge-instance *app*)
                                                                    search-pattern
                                                                    (orientation-delta (current-funge-instance *app*))
                                                                    (least-coord (current-funge-instance *app*))))))
                                    (if found
                                        (print-to-mini-buffer
                                         (format nil "Found: ~A Press <Ctrl-s> to jump to next occurence."
                                                 (length found)))
                                        (print-to-mini-buffer "String not found! (Hint: Check orientation delta?!)"))
                                    (setf (search-result (current-funge-instance *app*))
                                          found))))))
  ;; goto
  ((#. (char-code #\esc) #. (char-code #\g)) goto
                                             (ask-via-minibuffer (new-position "Goto (enter f.e. (0 0))"
                                                                  :widget-class 'mini-buffer-file-select)
                                               (aif (parse-input-vector new-position)
                                                    (progn
                                                      (setf (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
                                                            it)
                                                      (handle-cursor-if-out-of-view (current-funge-instance *app*)))
                                                    (print-to-mini-buffer "Error - no valid destionation vector."))))
  ;; enter custom orientation
  ((#. (char-code #\esc) #. (char-code #\o)) custom-orientation
                                             (ask-via-minibuffer (new-position "New orientation (enter f.e. (1 0))"
                                                                  :widget-class 'mini-buffer-file-select)
                                               (aif (parse-input-vector new-position)
                                                    (progn
                                                      (setf (orientation-delta (current-funge-instance *app*))
                                                            it)
                                                      (handle-cursor-if-out-of-view (current-funge-instance *app*)))
                                                    (print-to-mini-buffer "Error - no valid orientation vector."))))
  ;; undo
  ((24 #.(char-code #\u)) undo
                          (let ((undo-instruction
                                  (pop (undo-list
                                        (current-funge-instance *app*)))))
                            (if undo-instruction
                                (execute-undo-instruction undo-instruction)
                                ;; (cond
                                ;;   ((equal (car undo-instruction) 'insert)
                                ;;    (setf (cell* (current-funge-instance *app*)
                                ;;                 (second undo-instruction))
                                ;;          (third undo-instruction))
                                ;;    (unless (undo-list (current-funge-instance *app*))
                                ;;      (setf (has-changed-p (current-funge-instance *app*))
                                ;;            nil)))
                                ;;   ((equal (car undo-instruction) 'insert-funge)
                                ;;    (copy-funge-space (third undo-instruction)
                                ;;                      (current-funge-instance *app*)
                                ;;                      :target-offset (second undo-instruction))))
                                (print-to-mini-buffer "No further undo information."))))
  ;; scrolling
  ((#. charms/ll:key_down) scroll-down
                           (when (> (dimensions (current-funge-instance *app*)) 1)
                             (incf (y (funge-space-offset (current-funge-instance *app*))))))
  ((#. charms/ll:key_up) scroll-up
                         (when (> (dimensions (current-funge-instance *app*)) 1)
                           (decf (y (funge-space-offset (current-funge-instance *app*))))))
  ((#. charms/ll:key_right) scroll-right
                            (incf (x (funge-space-offset (current-funge-instance *app*)))))
  ((#. charms/ll:key_left) scroll-left
                           (decf (x (funge-space-offset (current-funge-instance *app*)))))
  ((#.(char-code #\Syn)) scroll-page-down
                         (when (> (dimensions (current-funge-instance *app*)) 1)
                           (incf (y (funge-space-offset (current-funge-instance *app*)))
                                 (- (tui:height (tui:window :editor))
                                    2))))
  ((#.(char-code #\esc) #.(char-code #\v)) scroll-page-up
                                           (when (> (dimensions (current-funge-instance *app*)) 1)
                                             (decf (y (funge-space-offset (current-funge-instance *app*)))
                                                   (- (tui:height (tui:window :editor))
                                                      2))))
  ((#.charms/ll:key_ppage) key-level-window-up
                           (when (> (dimensions (current-funge-instance *app*)) 2)
                             (decf (third (funge-space-offset (current-funge-instance *app*))))))
  ((#.charms/ll:key_npage) key-level-window-down
                           (when (> (dimensions (current-funge-instance *app*)) 2)
                             (incf (third (funge-space-offset (current-funge-instance *app*))))))
  ((1) jump-start-of-line ;; ctrl-a
       (let ((curr-curs (actual-cell-coords-beyond-cursor (current-funge-instance *app*))))
         (setf (x curr-curs)
               (x (least-coord (current-funge-instance *app*))))
         (setf (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
               curr-curs)
         (handle-cursor-if-out-of-view (current-funge-instance *app*))))
  ((5) jump-end-of-line ;; ctrl-e
       (let ((curr-curs (actual-cell-coords-beyond-cursor (current-funge-instance *app*))))
         (setf (x curr-curs)
               (x (greatest-coord (current-funge-instance *app*))))
         (setf (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
               curr-curs)
         (handle-cursor-if-out-of-view (current-funge-instance *app*))))
  ((#.(char-code #\esc) #.(char-code #\<)) jump-start
                                           (setf (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
                                                 (least-coord (current-funge-instance *app*)))
                                           (handle-cursor-if-out-of-view (current-funge-instance *app*)))
  ((#.(char-code #\esc)#.(char-code #\>)) jump-end
                                          (setf (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
                                                (greatest-coord (current-funge-instance *app*)))
                                          (handle-cursor-if-out-of-view (current-funge-instance *app*)))
  ;; orientation
  ((#.(char-code #\esc) 16) switch-orientation-north
                            (when (> (dimensions (current-funge-instance *app*)) 1)
                              (setf (orientation-delta
                                     (current-funge-instance *app*))
                                    (let ((l (make-n-list (dimensions
                                                           (current-funge-instance *app*)))))
                                      (setf (y l) -1)
                                      l))))
  ((#.(char-code #\esc) 14) switch-orientation-south
                            (when (> (dimensions (current-funge-instance *app*)) 1)
                              (setf (orientation-delta
                                     (current-funge-instance *app*))
                                    (let ((l (make-n-list (dimensions
                                                           (current-funge-instance *app*)))))
                                      (setf (y l) 1)
                                      l))))
  ((#.(char-code #\esc) 2) switch-orientation-west
                           (setf (orientation-delta
                                  (current-funge-instance *app*))
                                 (let ((l (make-n-list (dimensions
                                                        (current-funge-instance *app*)))))
                                   (setf (x l) -1)
                                   l)))
  ((#.(char-code #\esc) 6) switch-orientation-east
                           (setf (orientation-delta
                                  (current-funge-instance *app*))
                                 (let ((l (make-n-list (dimensions
                                                        (current-funge-instance *app*)))))
                                   (setf (x l) 1)
                                   l)))
  ;; move cursor
  ((6) key-move-cursor-right
       (incf (x (cursor (current-funge-instance *app*))))
       (handle-cursor-if-out-of-view (current-funge-instance *app*)))
  ((2) key-move-cursor-left
       (decf (x (cursor (current-funge-instance *app*))))
       (handle-cursor-if-out-of-view (current-funge-instance *app*)))
  ((16) key-move-cursor-up
        (when (> (dimensions (current-funge-instance *app*)) 1)
          (decf (y (cursor (current-funge-instance *app*))))
          (handle-cursor-if-out-of-view (current-funge-instance *app*))))
  ((14) key-move-cursor-down
        (when (> (dimensions (current-funge-instance *app*)) 1)
          (incf (y (cursor (current-funge-instance *app*))))
          (handle-cursor-if-out-of-view (current-funge-instance *app*))))
  ;; file create/load/save
  ((24 #.(char-code #\Ack)) load-or-create-file
                            (find-funge-instance))
  ((24 #.(char-code #\Dc3)) save-current-funge-instance
                            (save-funge-instance))
  ((24 #.(char-code #\Etb)) write-funge-instance
                            (write-funge-instance))
  ((24 #.(char-code #\~)) set-current-directory
                          (set-current-funge-directory))
  ;; kill buffer
  ((24 #.(char-code #\k)) kill-buffer
                          (kill-funge-instance))
  ;;switch buffer
  ((24 #.(char-code #\b)) switch-buffer
                          (switch-funge-instance)
                          (tui:repaint (tui:window :thread)))
  ((24 #.(char-code #\Stx)) switch-fast-last-buffer
                            (aif (second (funge-instances *app*))
                                 (progn
                                   (setf (second (funge-instances *app*))
                                         (first (funge-instances *app*)))
                                   (setf (first (funge-instances *app*))
                                         it)))
                            (tui:repaint (tui:window :thread)))
  ;; running / debugging funge-instance
  ((24 #.(char-code #\g)) reset-funge-instance
                          (reset-ip)
                          (tui:repaint (tui:window :thread)))
  ((#.charms/ll:key_ic) toggle-insert-mode
                        (cond ((equal (insert-mode (current-funge-instance *app*))  :overwrite)
                               (setf (insert-mode (current-funge-instance *app*)) :insert))
                              ((equal (insert-mode (current-funge-instance *app*))  :insert)
                               (setf (insert-mode (current-funge-instance *app*)) :shift))
                              ((equal (insert-mode (current-funge-instance *app*))  :shift)
                               (setf (insert-mode (current-funge-instance *app*)) :overwrite))))
  ((24 #.(char-code #\s)) run-single-tick
                          (tick (current-funge-instance *app*))
                          (log:debug (instruction-pointer (first (threads (current-funge-instance *app*)))))
                          (log:debug (funge::lifo (first (stack-stack (first (threads (current-funge-instance *app*)))))))
                          (tui:repaint (tui:window :thread))
                          (tui:do-update))
  ((24 #.(char-code #\r)) run
                          (tui:nodelay t)
                          (setf *running-funge-instance-toggle-debug* t)
                          (tui:event-loop :custom-event-map *running-funge-instance*
                                          :func-inner-event (lambda ()
                                                              (send-standard-output-to-output-window)
                                                              (when *running-funge-instance-toggle-debug*
                                                                (tui:repaint (tui:window :editor)))
                                                              (tui:repaint (tui:window :output))
                                                              (sleep *running-funge-instance-execution-delay*))
                                          :exit-condition exit-running-funge-instance)
                          (tui:nodelay nil)
                          (print-to-mini-buffer "Running funge-instance stopped."))
  ((24 #.(char-code #\R)) run-without-debug
                          (tui:nodelay t)
                          (setf *running-funge-instance-toggle-debug* nil)
                          (tui:event-loop :custom-event-map *running-funge-instance*
                                          :func-inner-event (lambda ()
                                                              (send-standard-output-to-output-window)
                                                              (when *running-funge-instance-toggle-debug*
                                                                (tui:repaint (tui:window :editor)))
                                                              (tui:repaint (tui:window :output))))
                          (tui:nodelay nil)
                          (print-to-mini-buffer "Running funge-instance stopped."))
  ((24 #. (char-code #\d)) toggle-breakpoint
                           (let ((current-pos (actual-cell-coords-beyond-cursor (current-funge-instance *app*))))
                             (if (find current-pos (breakpoints (current-funge-instance *app*)) :test #'equal)
                                 (setf (breakpoints (current-funge-instance *app*))
                                       (remove current-pos (breakpoints (current-funge-instance *app*)) :test #'equal))
                                 (push (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
                                       (breakpoints (current-funge-instance *app*))))))
  ((24 #. (char-code #\D)) remove-all-breakpoints
                           (setf (breakpoints (current-funge-instance *app*))
                                 nil))
  ;; general
  ((24 #. (char-code #\Page)) clear-output-window
                              (tui:scroll-up-to-begining (tui:window :output))
                              (setf (fill-pointer (tui:buffer (tui:window :output))) 0)
                              (setf *output-line-x* 1
                                    *output-line-y* 1)
                              (tui:repaint (tui:window :output)))
  ((or (#. (charms/ll:key_fn 10))
       (#.(char-code #\esc) #.(char-code #\esc))
       (#.(char-code #\esc) #.(char-code #\m))) raise-menu
                                                (tui:activate-widget (tui:window :editor)
                                                                     (tui:widget :editor :editor-menu)))
  ((24 #. (char-code #\o)) switch-to-thread-window
                           (tui:set-active (tui:window :thread)))
  ;; quit
  ((24 #. (char-code #\Etx)) quit
                             (signal 'tui:exit-event-loop))
  ;; start/stop slime
  ((24 #. (char-code #\&)) start/stop-slime
                           #+ (or sbcl ccl)
                           (if *swank-server*
                               nil
                               (progn
                                 (setf *swank-server*
                                       (swank:create-server :port 5555 :dont-close t))
                                 (print-to-mini-buffer "Swank startet at port 5555."))))
  ((#. (char-code #\Etx)) test
                          (print-to-mini-buffer "teste mal so...")))

(tui:defkeymap *running-funge-instance*
    (key)
  ((#.charms/ll:ERR) do-tick
   (if (some #'identity
             (loop for ithread in (threads (current-funge-instance *app*))
                collect (find (instruction-pointer ithread)
                              (breakpoints (current-funge-instance *app*))
                              :test #'equal)))
       (progn
         (print-to-mini-buffer "Breakpoint reached - funge-instance stopped.")
         (signal 'exit-running-funge-instance))
       (progn
         (tick (current-funge-instance *app*))
         (when *running-funge-instance-toggle-debug*
           (tui:repaint (tui:window :thread))))))
  ((#. (char-code #\o)) toogle-debug
   (if *running-funge-instance-toggle-debug*
       (setf *running-funge-instance-toggle-debug* nil)
       (setf *running-funge-instance-toggle-debug* t)))
  ((#. (char-code #\+)) increase-execution-delay
   (incf *running-funge-instance-execution-delay* 0.1)
   (print-to-mini-buffer (format nil "Execution delay: ~A seconds."
                                 *running-funge-instance-execution-delay*)))
  ((#. (char-code #\-)) decrease-execution-delay
   (if (> (- *running-funge-instance-execution-delay* 0.1) 0)
       (decf *running-funge-instance-execution-delay* 0.1)
       (progn
         (setf *running-funge-instance-execution-delay* 0)))
   (print-to-mini-buffer (format nil "Execution delay: ~A seconds."
                                 *running-funge-instance-execution-delay*)))
  ((#. charms/ll:key_down) scroll-down
   (incf (y (funge-space-offset (current-funge-instance *app*)))))
  ((#. charms/ll:key_up) scroll-up
   (decf (y (funge-space-offset (current-funge-instance *app*)))))
  ((#. charms/ll:key_right) scroll-right
   (incf (x (funge-space-offset (current-funge-instance *app*)))))
  ((#. charms/ll:key_left) scroll-left
   (decf (x (funge-space-offset (current-funge-instance *app*)))))
  ((#.(char-code #\Syn)) scroll-page-down
   (incf (y (funge-space-offset (current-funge-instance *app*)))
         (- (tui:height (tui:window :editor))
            2)))
  ((#.(char-code #\esc) #.(char-code #\v)) scroll-page-up
   (decf (y (funge-space-offset (current-funge-instance *app*)))
         (- (tui:height (tui:window :editor))
            2)))
  ((1) jump-start-of-line ;; ctrl-a
   (let ((curr-curs (actual-cell-coords-beyond-cursor (current-funge-instance *app*))))
     (setf (x curr-curs)
           (x (least-coord (current-funge-instance *app*))))
     (setf (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
           curr-curs)
     (handle-cursor-if-out-of-view (current-funge-instance *app*))))
  ((5) jump-end-of-line ;; ctrl-e
   (let ((curr-curs (actual-cell-coords-beyond-cursor (current-funge-instance *app*))))
     (setf (x curr-curs)
           (x (greatest-coord (current-funge-instance *app*))))
     (setf (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
           curr-curs)
     (handle-cursor-if-out-of-view (current-funge-instance *app*))))
  ((#.(char-code #\esc) #.(char-code #\<)) jump-start
   (setf (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
         (least-coord (current-funge-instance *app*)))
   (handle-cursor-if-out-of-view (current-funge-instance *app*)))
  ((#.(char-code #\esc)#.(char-code #\>)) jump-end
   (setf (actual-cell-coords-beyond-cursor (current-funge-instance *app*))
         (greatest-coord (current-funge-instance *app*)))
   (handle-cursor-if-out-of-view (current-funge-instance *app*)))
  ((#. (char-code #\s)) stop
   (signal 'exit-running-funge-instance)))

(defparameter *editor-menu*
  `(("File"
     ("Load/Create..." "Ctrl-x f" find-funge-instance)
     ("Save" "Ctrl-x s" save-funge-instance)
     ("Write..." "Ctrl-x Ctrl-w" write-funge-instance)
     ("Save all" "Ctrl-x S" nil)
     (-)
     ("Switch funge-space..."
      "Ctrl-x b"
      switch-funge-instance)
     ("Fast switch last funge-space"
      "Ctrl-x Ctrl-b"
      ,(lambda ()
         (aif (second (funge-instances *app*))
              (progn
                (setf (second (funge-instances *app*))
                      (first (funge-instances *app*)))
                (setf (first (funge-instances *app*))
                      it)))))
     ("Kill buffer..." "Ctrl-x k" kill-funge-instance)
     (-)
     ("Quit" "Ctrl-x c"
             ,(lambda ()
                (signal 'tui:exit-event-loop))))
    ("Edit"
     ("Set Mark" "Ctrl-Space"
                 ,(lambda ()
                    (*editor-event-map*-set-mark nil)))
     ("Undo" "Ctrl-x u" ,(lambda ()
                           (*editor-event-map*-undo nil)))
     ("Cut" "Ctrl-w" ,(lambda ()
                        (*editor-event-map*-kill-region nil)))
     ("Copy" "Meta-w" ,(lambda ()
                         (*editor-event-map*-copy-region-to-clipboard nil)))
     ("Paste (yank)" "Ctrl-y" ,(lambda ()
                                 (*editor-event-map*-yank nil)))
     ("Yank-pop" "Meta-y"
                 ,(lambda ()
                    (*editor-event-map*-yank-next-from-killring nil)))
     (-)
     ("Change orientation to east" "Ctrl-Meta-f")
     ("Change orientation to west" "Ctrl-Meta-b")
     ("Change orientation to north" "Ctrl-Meta-p")
     ("Change orientation to south" "Ctrl-Meta-n")
     ("Enter custom orientation..." "Meta-o" ,(lambda ()
                                                (*editor-event-map*-custom-orientation nil)))
     (-)
     ("Show value under cursor" "Ctrl-x =" ,(lambda ()
                                              (*editor-event-map*-show-cell-value nil)))
     ("Search..."
      "Ctrl-s"
      ,(lambda ()
         (*editor-event-map*-search nil)))
     ("Goto..." "Meta-g" ,(lambda ()
                            (*editor-event-map*-goto nil))))
    ("Funge instance"
     ("Run single next instruction"
      "Ctrl-x s"
      ,(lambda ()
         (*editor-event-map*-run-single-tick nil)))
     ("Start watching"
      "Ctrl-x r"
      ,(lambda ()
         (*editor-event-map*-run nil)))
     ("Start output only"
      "Ctrl-x R"
      ,(lambda ()
         (*editor-event-map*-run-without-debug nil)))
     ("Toggle breakpoint"
      "Ctrl-x d"
      ,(lambda ()
         (*editor-event-map*-toggle-breakpoint nil)))
     ("Remove all breakpoints"
      "Ctrl-x D"
      ,(lambda ()
         (*editor-event-map*-remove-all-breakpoints nil)))
     ("Reset ip and threads"
      "Ctrl-x g"
      ,(lambda ()
         (*editor-event-map*-reset-funge-instance nil)))
     ("Change current directory..."
      "Ctrl-x ~"
      set-current-funge-directory)
     ("Recalculate borders"
      ""
      ,(lambda ()
         (funge:recalc-borders (current-funge-instance *app*))
         (tui:repaint (tui:window :thread))
         (tui:refresh (tui:window :thread) ))))
    ("Window"
     ("Next window" "Ctrl-x o"
                    ,(lambda ()
                       (*editor-event-map*-switch-to-thread-window nil)))
     (-)
     ("Zoom/unzoom editor window"
      "Ctrl-x z"
      ,(lambda ()
         (*editor-event-map*-zoom-editor-window nil)))
     ("Increase editor window vertical"
      "Ctrl-x +"
      ,(lambda ()
         (*editor-event-map*-increase-editor-window-ver nil)))
     ("Decrease editor window vertical"
      "Ctrl-x -"
      ,(lambda ()
         (*editor-event-map*-decrease-editor-window-ver nil)))
     ("Increase editor window horizontal"
      "Ctrl-x }"
      ,(lambda ()
         (*editor-event-map*-increase-editor-window-hor nil)))
     ("Decrease editor window horizontal"
      "Ctrl-x {"
      ,(lambda ()
         (*editor-event-map*-decrease-editor-window-hor nil)))
     (-)
     ("Clear output window"
      "Ctrl-x Ctrl-l"
      ,(lambda ()
         (*editor-event-map*-clear-output-window nil))))
    ("Help"
     ("Befunge98" "TODO")
     ("Befunge93" "TODO")
     (-)
     ("Start Swank" "Ctrl-x &" ,(lambda ()
                                  (*editor-event-map*-start/stop-slime nil)))
     (-)
     ("About toadstool" "Ctrl-h a" about-toadstool))))

(tui:defkeymap *thread-event-map*
    (key)
  ((#. charms/ll:key_up) keyscroll-up
   (tui:scroll-up (tui:window :thread)))
  ((#. charms/ll:key_down) keyscroll-down
   (tui:scroll-down (tui:window :thread)))
  ((#. charms/ll:key_right) keyscroll-right
   (tui:scroll-right (tui:window :thread)))
  ((#. charms/ll:key_left) keyscroll-left
   (tui:scroll-left (tui:window :thread)))
  ((#. (char-code #\esc) #. (char-code #\<)) scroll-beginning
   (tui:scroll-up-to-begining (tui:window :thread)))
  ((#. (char-code #\esc) #. (char-code #\>)) scroll-ending
   (tui:scroll-down-to-end (tui:window :thread)))
  ((#. (char-code #\Soh)) scroll-left-begining
   (tui:scroll-to-left-begining (tui:window :thread)))
  ((#. (char-code #\Enq)) scroll-right-begining
   (tui:scroll-to-right-end (tui:window :thread)))
  ((24 #. (char-code #\o)) switch-to-output-window
   (tui:set-active (tui:window :output))))

(tui:defkeymap *output-event-map*
    (key)
  ((#. charms/ll:key_up) keyscroll-up
   (tui:scroll-up (tui:window :output)))
  ((#. charms/ll:key_down) keyscroll-down
   (tui:scroll-down (tui:window :output)))
  ((#. charms/ll:key_right) keyscroll-right
   (tui:scroll-right (tui:window :output)))
  ((#. charms/ll:key_left) keyscroll-left
   (tui:scroll-left (tui:window :output)))
  ((#. (char-code #\esc) #. (char-code #\<)) scroll-beginning
   (tui:scroll-up-to-begining (tui:window :output)))
  ((#. (char-code #\esc) #. (char-code #\>)) scroll-ending
   (tui:scroll-down-to-end (tui:window :output)))
  ((#. (char-code #\Soh)) scroll-left-begining
   (tui:scroll-to-left-begining (tui:window :output)))
  ((#. (char-code #\Enq)) scroll-right-begining
   (tui:scroll-to-right-end (tui:window :output)))
  ((24 #. (char-code #\o)) switch-to-editor-window
   (tui:set-active (tui:window :editor))))

(defun print-to-mini-buffer (string)
  (tui:clear (tui:window :mini-buffer))
  (tui:wprint
   (tui:window :mini-buffer)
   0 0 string)
  (tui:repaint (tui:window :mini-buffer))
  (setf (clear-mini-buffer *app*) t))

(defun calculate-visible-area-of-region (fs)
  (if (> (dimensions fs) 1)
      (let* ((first-corner
              (list
               (let ((x (x (--vector (mark fs) (funge-space-offset fs)))))
                 (cond
                   ((< x 0) 0)
                   ((> x (- (tui:width (tui:window :editor)) 2))
                    (- (tui:width (tui:window :editor)) 2))
                   (t x)))
               (let ((y (y (--vector (mark fs) (funge-space-offset fs)))))
                 (cond
                   ((< y 0) 0)
                   ((> y (- (tui:height (tui:window :editor)) 2))
                    (- (tui:height (tui:window :editor)) 2))
                   (t y)))))
             (second-corner
              (cursor fs))
             (corners
              (sort (list first-corner
                          second-corner
                          (list (x first-corner)
                                (y second-corner))
                          (list (x second-corner)
                                (y first-corner)))
                    #'<
                    :key (lambda (corner)
                           (+ (x corner) (y corner))))))
        (list (first corners) 
              (fourth corners)))
      (let ((first-corner
             (list
              (let ((x (x (--vector (mark fs) (funge-space-offset fs)))))
                (cond
                  ((< x 0) 0)
                  ((> x (- (tui:width (tui:window :editor)) 2))
                   (- (tui:width (tui:window :editor)) 2))
                  (t x)))))
            (second-corner (cursor fs)))
        (sort (list first-corner
                    second-corner)
              #'<
              :key #'car))))

(defparameter +escape-char+ #\PILCROW_SIGN)

(defun safe-code-char (code)
  "Checks wheter char is printable in one cell, otherwise it prints an
  escape char."
  (if (and (>= code 32)
           (<= code 126))
      (code-char code)
      +escape-char+))

(defun draw-editor-window (self fi)
  (let ((unefunge-row (floor (/ (tui:height self) 2))))
    ;; print clip of funge-space
    (if (> (dimensions fi) 1)
        (loop
          for iy from 0 to (- (tui:height self) 3)
          do 
             (tui:wprint
              self
              1 (1+ iy)
              (with-output-to-string (s)
                (loop
                  for ix from 0 to (- (tui:width self)
                                      3)
                  do 
                     (write-char 
                      (safe-code-char (actual-cell-beyond-cursor-xy fi ix iy)) 
                      s)))))
        (loop
          for iy from 0 to (- (tui:height self) 2)
          do 
             (if (= iy unefunge-row)
                 (tui:wprint
                  self
                  1 iy
                  (with-output-to-string (s)
                    (loop
                      for ix from 0 to (- (tui:width self)
                                          2)
                      do 
                         (write-char 
                          (safe-code-char (actual-cell-beyond-cursor-xy fi ix iy)) 
                          s))))
                 ;; (loop
                 ;;    for ix from 0 to (- (tui:width self)
                 ;;                        2)
                 ;;    do
                 ;;      (tui:wprint self ix iy #+sbcl"░" #-sbcl" ")
                 ;;    ;; (write-char 
                 ;;    ;;  #\X
                 ;;    ;;  s)
                 ;;      )
                 (tui:wprint self 1 iy (with-output-to-string (s)
                                         (loop for iii from 1 to (tui:width self)
                                               do
                                                  (princ #+sbcl"░" #-sbcl" " s)))))))
    ;; print breakpoints
    (if (> (dimensions fi) 1)
        (loop for i in (breakpoints (current-funge-instance *app*))
              do
                 (let ((x-ip (- (x i)
                                (x (funge-space-offset fi))))
                       (y-ip (- (y i)
                                (y (funge-space-offset fi)))))
                   (when (and (>= x-ip 0)
                              (<= x-ip (- (tui:width self) 3))
                              (>= y-ip 0)
                              (<= y-ip (- (tui:height self) 3))
                              (equal (nthcdr 2 (funge-space-offset (current-funge-instance *app*)))
                                     (nthcdr 2 i)))
                     (tui:with-wattr self
                         ((charms/ll:color-pair 4))
                       (tui:wprint
                        self
                        (1+ x-ip)
                        (1+ y-ip)
                        (string
                         (safe-code-char
                          (actual-cell-beyond-cursor-xy
                           fi
                           x-ip
                           y-ip))))))))
        (loop for i in (breakpoints (current-funge-instance *app*))
              do
                 (let ((x-ip (- (x i)
                                (x (funge-space-offset fi)))))
                   (when (and (>= x-ip 0)
                              (<= x-ip (- (tui:width self) 3)))
                     (tui:with-wattr self
                         ((charms/ll:color-pair 4))
                       (tui:wprint
                        self
                        (1+ x-ip)
                        unefunge-row
                        (string
                         (safe-code-char
                          (actual-cell-beyond-cursor-xy
                           fi
                           x-ip
                           nil)))))))))
    ;; print instruction pointers (IP)
    (if (> (dimensions fi) 1)
        (loop for ithread in (threads fi)
              do
                 (let ((x-ip (- (x (instruction-pointer ithread))
                                (x (funge-space-offset fi))))
                       (y-ip (- (y (instruction-pointer ithread))
                                (y (funge-space-offset fi)))))
                   (when (and (>= x-ip 0)
                              (<= x-ip (- (tui:width self) 3))
                              (>= y-ip 0)
                              (<= y-ip (- (tui:height self) 3))
                              (equal (nthcdr 2 (funge-space-offset (current-funge-instance *app*)))
                                     (nthcdr 2 (instruction-pointer ithread))))
                     (tui:with-wattr self
                         ((charms/ll:color-pair 7))
                       (tui:wprint
                        self
                        (1+ x-ip)
                        (1+ y-ip)
                        (string
                         (safe-code-char
                          (actual-cell-beyond-cursor-xy
                           fi
                           x-ip
                           y-ip))))))))
        (loop for ithread in (threads fi)
              do
                 (let ((x-ip (- (x (instruction-pointer ithread))
                                (x (funge-space-offset fi)))))
                   (when (and (>= x-ip 0)
                              (<= x-ip (- (tui:width self) 3)))
                     (tui:with-wattr self
                         ((charms/ll:color-pair 7))
                       (tui:wprint
                        self
                        (1+ x-ip)
                        unefunge-row
                        (string
                         (safe-code-char
                          (actual-cell-beyond-cursor-xy
                           fi
                           x-ip
                           nil)))))))))
    ;; print last search-result
    (if (> (dimensions fi) 1)
        (loop for ifound in (search-result fi)
              do
                 (let ((x-ip (- (x ifound)
                                (x (funge-space-offset fi))))
                       (y-ip (- (y ifound)
                                (y (funge-space-offset fi)))))
                   (when (and (>= x-ip 0)
                              (<= x-ip (- (tui:width self) 3))
                              (>= y-ip 0)
                              (<= y-ip (- (tui:height self) 3))
                              (equal (nthcdr 2 (funge-space-offset (current-funge-instance *app*)))
                                     (nthcdr 2 ifound)))
                     (tui:with-wattr self
                         ((charms/ll:color-pair 6))
                       (tui:wprint
                        self
                        (1+ x-ip)
                        (1+ y-ip)
                        (string
                         (safe-code-char
                          (actual-cell-beyond-cursor-xy
                           fi
                           x-ip
                           y-ip))))))))
        (loop for ifound in (search-result fi)
              do
                 (let ((x-ip (- (x ifound)
                                (x (funge-space-offset fi)))))
                   (when (and (>= x-ip 0)
                              (<= x-ip (- (tui:width self) 3)))
                     (tui:with-wattr self
                         ((charms/ll:color-pair 6))
                       (tui:wprint
                        self
                        (1+ x-ip)
                        unefunge-row
                        (string
                         (safe-code-char
                          (actual-cell-beyond-cursor-xy
                           fi
                           x-ip
                           nil)))))))))
    ;; print region
    (when (mark-aktive-p fi)
      (if (> (dimensions fi) 1)
          (let ((region (calculate-visible-area-of-region fi)))
            (loop for ix from (x (first region)) to (x (second region))
                  do
                     (loop for iy from (y (first region)) to (y (second region))
                           do
                              (tui:with-wattr self
                                  (charms/ll:a_reverse)
                                (tui:wput self
                                          (1+ ix) (1+ iy)
                                          (safe-code-char
                                           (actual-cell-beyond-cursor-xy fi ix iy)))))))
          (let ((region (calculate-visible-area-of-region fi)))
            (loop for ix from (x (first region)) to (x (second region))
                  do
                     (tui:with-wattr self
                         (charms/ll:a_reverse)
                       (tui:wput self
                                 (1+ ix) unefunge-row
                                 (safe-code-char
                                  (actual-cell-beyond-cursor-xy fi ix nil))))))))
    ;; finally print cursor, we need to see him in all cases!
    (if (> (dimensions fi) 1)
        (tui:with-wattr self ((charms/ll:color-pair 1) ;; charms/ll:a_reverse
                              charms/ll:a_blink)
          (tui:wprint self
                      (1+ (x (cursor fi)))
                      (1+ (y (cursor fi)))
                      (string (safe-code-char
                               (actual-cell-beyond-cursor-xy
                                fi
                                (x (cursor fi))
                                (y (cursor fi)))))))
        (tui:with-wattr self ((charms/ll:color-pair 1) ;; charms/ll:a_reverse
                              charms/ll:a_blink)
          (tui:wprint self
                      (1+ (x (cursor fi)))
                      unefunge-row
                      (string (safe-code-char
                               (actual-cell-beyond-cursor-xy
                                fi
                                (x (cursor fi))
                                nil))))))))

(defun draw-thread-window (self fi)
  (setf (fill-pointer (tui:buffer self)) 0)
  (tui:wprint-append self (format nil "Ticks: ~D" (no-of-ticks fi)))
  (tui:wprint-append self (format nil "Num of thrd: ~D" (length (threads fi))))
  (tui:wprint-append self (format nil "c:~A/~A"
                                  (least-coord fi)
                                  (greatest-coord fi)))
  (tui:wprint-append self
                     (subseq
                      #+sbcl"----------------------------------------" ;"────────────────────────────────────────"
                      #+ccl"----------------------------------------"
                      #+ecl"----------------------------------------"
                      #+clisp"----------------------------------------"
                      0 (- (tui:width self) 2)))
  (loop for ithread in (threads fi)
        do
           (tui:wprint-append self (format nil "ID ~D" (id ithread)))
           (tui:wprint-append self (format nil "storg-ofs ~A"
                                           (storage-offset ithread)))
           (tui:wprint-append self (format nil "IP ~A"
                                           (instruction-pointer ithread)))
           (tui:wprint-append self (format nil "delta ~A" (delta ithread)))
           (tui:wprint-append self (format nil "strg-mode ~A"
                                           (string-mode ithread)))
           (tui:wprint-append self (format nil " Num of stck: ~D"
                                           (length (stack-stack ithread))))
           (loop
             for istack in (stack-stack ithread)
             for nstack = 1 then (incf nstack)
             do
                (loop
                  for stack-element in (reverse (lifo istack))
                  for counter = 0 then (incf counter)
                  do
                     (tui:wprint-append
                      self
                      (format nil "~D/~4,'0D:~5D ~3X ~C" nstack counter stack-element stack-element (safe-code-char stack-element)))))
           (tui:wprint-append self
                              (subseq "- - - - - - - - - - - - - - - - -"
                                      0 (- (tui:width self) 2))))
  (tui:wprint-append self "     " ))

(defun toadstool ()
  (let ((funge-instance (make-instance 'funge-instance-in-editor
                                       :full-file-name nil)))

    (setf *app* (make-instance 'app
                               :funge-instances (list funge-instance)))
    (tui:with-tui
      (tui:init-screen)
      (setf (editor-window *app*)
            (make-instance 'editor-window
                           :name :editor
                           :title "Editor"
                           :title-print-function
                           (lambda (title)
                             (declare (ignore title))
                             (format
                              nil "v= ~A = ~A~A= Cur: ~A = Orient: ~A = Mode: ~A ="
                              (funge-space-offset
                               (current-funge-instance *app*))
                              (name (current-funge-instance *app*))
                              (if (has-changed-p (current-funge-instance *app*))
                                  "*" " ")
                              (actual-cell-coords-beyond-cursor
                               (current-funge-instance *app*))
                              (orientation-delta
                               (current-funge-instance *app*))
                              (cond
                                ((equal (insert-mode (current-funge-instance *app*)) :insert)
                                 "i")
                                ((equal (insert-mode (current-funge-instance *app*)) :overwrite)
                                 "o")
                                ((equal (insert-mode (current-funge-instance *app*)) :shift)
                                 "s"))))
                           :start-left-dist 0 :start-top-dist 0
                           :end-right-dist 21 :end-bottom-dist 8
                           :draw-func (lambda (self)
                                        (draw-editor-window
                                         self
                                         (current-funge-instance *app*)))
                           :event-map *editor-event-map*))
      (setf (thread-window *app*)
            (make-instance 'thread-window
                           :name :thread
                           :title "Threads"
                           :start-right-dist 21 :start-top-dist 0
                           :end-right-dist 0 :end-bottom-dist 8
                           :draw-func (lambda (self)
                                        (draw-thread-window
                                         self
                                         (current-funge-instance *app*)))
                           :event-map *thread-event-map*))
      (setf (output-window *app*)
            (make-instance 'output-window
                           :name :output
                           :title "Output"
                           :start-left-dist 0 :start-bottom-dist 8
                           :end-right-dist 0 :end-bottom-dist 1
                           :draw-func nil
                           :event-map *output-event-map*))
      (setf (mini-buffer-window *app*)
            (make-instance 'mini-buffer-window
                           :name :mini-buffer
                           :start-left-dist 0 :start-bottom-dist 1
                           :end-right-dist 0 :end-bottom-dist 0
                           :draw-func nil
                           :event-map nil))
      (tui:set-active (tui:window :editor))
      (make-instance 'tui:menu
                     :window (tui:window :editor)
                     :name :editor-menu
                     :menu-structure *editor-menu*)
      (let ((*standard-output* (make-string-output-stream))
            (*output-line-x* 1)
            (*output-line-y* 1))
        (tui:event-loop :func-before-event
                        (lambda ()
                          (tui:repaint (tui:active-window tui:*screen*)))
                        :func-inner-event
                        (lambda ()
                          (send-standard-output-to-output-window)
                          (tui:repaint (tui:active-window tui:*screen*))
                          (when (not *toggle-zoom-editor-window*)
                            (tui:repaint (tui:window :thread))))
                        :func-after-event
                        (lambda ()
                          (when (clear-mini-buffer *app*)
                            (tui:clear (tui:window :mini-buffer))
                            (tui:mark-for-later-refresh
                             (tui:window :mini-buffer))
                            (setf (clear-mini-buffer *app*)
                                  nil))
                          ;; handle yank/killring
                          (if (previous-command-issued-p *app*)
                              (setf (previous-command-issued-p *app*) nil)
                              (setf (previous-command *app*) nil))
                          ;; (tui:repaint (tui:window :thread))
                          ))))))

(defun cl-user::toadstool ()
  (toadstool:toadstool))


(define-condition exit-about-window () ())
(tui:defkeymap *about-window*
    (key)
  (((32 126)) quit
   (signal 'exit-about-window)))

(defclass about-window (tui:centered-window
                        tui:titled-window
                        tui:widgeted-window
                        tui:bordered-window)
  ())

(defun about-toadstool ()
  (make-instance 'about-window
                 :name :about
                 :title "About toadstool"
                 :width 50 :height 17
                 :draw-func (lambda (self)
                              (tui:wprint self 3 2 "🍄 Toadstool - a (be)fungeIDE 0.1 alpha ;)")
                              (tui:wprint self 15 3 (format
                                                     nil
                                                     "Running funge ~A" funge:*funge-version*))
                              (tui:wprint self 5 5 "by Otto Diesenbacher-Reinmüller (okflo)")
                              (tui:wprint self 5 6 "   mailto:okflo@diesenbacher.net")
                              (tui:wprint self 5 7 " http://toadstool.diesenbacher.net/")
                              (tui:wprint self 3 9 "License: GPL V3 (please check \"LICENSE\")")
                              (tui:wprint self 3 11 (format nil "Lisp implementation: ~A"
                                                            (lisp-implementation-type)))
                              (tui:wprint self 3 12 (format nil "Implementation version: ~A"
                                                            (lisp-implementation-version)))
                              (tui:wprint self 3 13 (format nil "Playfield implementation: ~A"
                                                            (funge::playfield-type (current-funge-instance *app*)))))
                 
                 :event-map *about-window*)
  (make-instance 'tui:button :window (tui:window :about)
                 :name :exit-about
                 :x 17 :y 14
                 :content "Ok (Return)"
                 :func-to-call-on-act (lambda (self)
                                        (tui:set-active (tui:window :editor))
                                        (tui:kill (tui:mother-window self))
                                        (tui:mark-for-later-refresh (tui:window :output))
                                        (tui:mark-for-later-refresh (tui:window :editor))
                                        (tui:do-update)
                                        (tui:repaint tui:*screen*)))
  (tui:set-active (tui:window :about))
  (tui:activate-widget (tui:window :about) (tui:widget :about :exit-about))
  (tui:repaint (tui:window :about))
  ;; (tui:event-loop :exit-condition exit-about-window)


  ;; (tui:repaint tui:*screen*)
  )

(defun save-toadstool ()
  #+sbcl(sb-ext:save-lisp-and-die "/home/okflo/toadstool"
                                  :executable t
                                  :toplevel #'toadstool:toadstool)
  #+ccl (ccl:save-application "/home/okflo/toadstool"
                              :toplevel-function #'toadstool:toadstool
                              :prepend-kernel t)
  #+clisp (ext:saveinitmem "/home/okflo/toadstool"
                           :quiet t
                           :init-function #'toadstool:toadstool
                           :executable t
                           :norc t))
