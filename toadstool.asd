(in-package :cl-user)

(asdf:defsystem :toadstool
  :version "0.1"
  :depends-on (:funge :tui :cl-fad :swank :log4cl :log4cl.log4slime)
  :components ((:file "packages")
               (:file "utilities" :depends-on ("packages"))
               (:file "funge-space-in-editor" :depends-on ("utilities" "packages"))
               (:file "app" :depends-on ("packages" "funge-space-in-editor"))
               (:file "fungify" :depends-on ("packages" "funge-space-in-editor")))
  :build-operation "asdf:program-op"
  :build-pathname "toadstool.exe"
  :entry-point "toadstool:toadstool")

